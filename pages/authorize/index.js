var app = getApp();
var util = require('../../utils/util.js');
import {get, post, put, del} from '../../utils/network';
var that;
Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: qq.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function () {
	  
  },
  getUserInfo: function (e) {
    console.log(e)
	that = this;
	qq.login({
		success(res) {
			if (res.code) {
				let reqData = {
					appid:app.globalData.appid,
				    code: res.code,
				    encryptedData: e.detail.encryptedData,
				    iv: e.detail.iv,
					platform:'QQ'
				}
				post('auth/openuser/'+app.globalData.appid+'/register',reqData).then(res=>{
					console.log(res);
					app.globalData.token = res.token;
					app.globalData.userInfo = res.user;
					app.globalData.isLogin = true;
					that.setData({
						userInfo:res.user
					});
					qq.navigateBack();
				})
			} else {
				console.log('登录失败！' + res.errMsg)
			}
		}
	})	
    
  }
})
