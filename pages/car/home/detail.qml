<view class="top bgwhite">

    <view class="flex mt15" style="align-items:center;">
        <image class="user-img" src="../../../image//userinfo.png"   mode="aspectFill"></image>
        <view class="sub ml10 user-msg">
            <view>
                <text class="lineText">{{detail.nickname}}</text>
                <text class="grey f12 ml10">{{detail.createTime}}</text>
            </view>
        </view>
    </view>

</view>

<view class="content f18 bgwhite" >
    <text>{{detail.remark}}</text>
</view>

<!--浮动按钮 回到首页，导航 -->
<view class="backtop" >       
    <text bindtap="goHome" class="iconfont icon-homefill"></text>
    
</view>

<view class="bottom-btn-box">
    <view class="flex tc bgwhite gridXt">
        <!--评论点赞-->
        <view class="sub flex f20">
            <button class="sub share bottom-btn" open-type="share">
                <view class="iconfont icon-share"></view>
                <view class="f12 grey">分享</view>
            </button>                       
        </view>
        <!--邀请同行-->        
        <!-- <button class="contact-btn"  type="primary" data-mobile="{{mobile}}" form-type="submit" >邀请同行</button> -->
         <!--联系ta--> 
        <button class="contact-btn" style="background-color:#20C826;" type="primary" data-mobile="{{detail.mobile}}" catchtap="onCallPhoneTap" disabled="{{detail.mobile==''}}">打电话</button>
    </view>
</view>