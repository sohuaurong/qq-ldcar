<view class="container">
    <view class="top">
        <view class="nav_top">
            <input bindconfirm="searchRequest" bindinput="searchInput" class="{{searchData.length==0?'inputBg':''}}" confirmType="search" placeholder="搜索关键词(终点,手机号)" placeholderStyle="color:#130c0e" value="{{searchData}}"></input>
            <image src="" style="width:30rpx;height:30rpx;"></image>
        </view>
    </view>
</view>

 <!--顶部选项-->
<view class="top-title flex tc">
  <view class="title bgwhite sub transition-duration-150 {{tabIndex==0?'active':''}}" catchtap="onTabChangeTap" data-tab-index="0">人找车</view>
  <view class="title bgwhite sub transition-duration-150 {{tabIndex==1?'active':''}}" catchtap="onTabChangeTap" data-tab-index="1">车找人</view>
</view>

<!-- 导入模板 -->
<import src="tmpl-list-relase.qml" />
<template is="find-car-list" data="{{findCarList}}" wx:if="{{tabIndex==0}}"></template>
<template is="find-people-list" data="{{findPeopleList}}" wx:if="{{tabIndex==1}}"></template>

<!-- 没有数据-->
  <view class="page__bd">
    <view class="weui-loadmore" bindtap="onReachBottom" wx:if="{{loading}}">
      <view class="weui-loading"></view>
      <view class="weui-loadmore__tips">正在加载</view>
    </view>
    <view class="weui-loadmore weui-loadmore_line" wx:if="{{ tabIndex==0 && !hasMoreCar}}">
      <view class="weui-loadmore__tips weui-loadmore__tips_in-line">没有更多了~</view>
    </view>
    <view class="weui-loadmore weui-loadmore_line" wx:if="{{ tabIndex==1 && !hasMorePeople}}">
      <view class="weui-loadmore__tips weui-loadmore__tips_in-line">没有更多了~</view>
    </view>
  </view>