const _ = require('../../../utils/underscore');
var app = getApp();
var util = require('../../../utils/util.js');
var dialog = require('../../../utils/dialog.js');
import {get, post, put, del} from '../../../utils/network';
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
		searchData: "",
		showSearch: true,
		tabIndex: 1,//选项卡索引
		isEmpty: false,//数据是否为空
		hasMoreCar: true,//是否还有更多人找车数据
		hasMorePeople: true,//是否还有更多车找人数据
		count:5,//每页请求条数
		findCarPage:0,//人找车的当前页码
		findPeoplePage:0,//车找人的当前页码
		findCarList:[],
		findPeopleList:[]
  },
	
  searchRequest: function(a) {
	console.info('sendrequest');
	that = this;
	that.setData({findCarPage:0,hasMoreCar:true});
	that.setData({findPeoplePage:0,hasMorePeople:true});
	that.getCarpoolList(1);
	that.getCarpoolList(2);
  },
	searchInput: function(a) {
	    0 === a.detail.cursor && this.setData({
	        showSearch: false
	    }), this.setData({
	        searchData: a.detail.value
	    });
	},
	onTabChangeTap:function(e){
		//切换选项卡(0 人找车，1车找人)
		const index = parseInt(e.currentTarget.dataset.tabIndex);
		this.setData({ tabIndex: index });
	},
	getCarpoolList:function(type){
		that = this;
		var requestPage = 0;
		if(type == 1){
			requestPage = that.data.findCarPage;
		}else if(type == 2){
			requestPage = that.data.findPeoplePage;
		}
		var formData = {
				type:type,
				keyword:that.data.searchData,
				page:requestPage,
				count:that.data.count
			};
		post('v1/simple/carpool/search',formData).then(data=>{
			console.info('请求成功条数:'+data.length);
			if(data.length == 0){
				that.setData({nomore:true});
				if(type == 1){
					that.setData({hasMoreCar:false});
				}else if(type == 2){
					that.setData({hasMorePeople:false});
				}
			}
			//统一处理数据
			that.onDataHandler(data);
			if(type == 1){//人找车
				that.setData({
					findCarPage:that.data.findCarPage+1,
					findCarList:requestPage == 0 ? data : that.data.findCarList.concat(data)
				});
				//console.info('人找车数据:'+JSON.stringify(that.data.findCarList));
			}else if(type == 2){//车找人
				that.setData({
					findPeoplePage:that.data.findPeoplePage+1,
					findPeopleList:requestPage == 0 ? data : that.data.findPeopleList.concat(data)
				});
				//console.info('车找人数据:'+JSON.stringify(that.data.findPeopleList));
			}
		})
	},
  /**
   * 生命周期函数--监听页面加载
   */
	onLoad: function (options) {
		that = this;
		that.getCarpoolList(1);
		that.getCarpoolList(2);
	},
	onDataHandler: function (data) {
		//数据处理
		_(data).map((item) => {
		  //item.distance = item.distance.toFixed(1);
		  //item.week = util.formatWeekTips(item.routeDate,new Date());
		  //item.formatDate = util.formatRoute(item.routeDate);
		  item.createTime = util.formatRoute(item.createTime);
		  return item;
		});
	 },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
	onPullDownRefresh:function(){		
		that = this;
		//重置为第一页
		var type = parseInt(that.data.tabIndex);
		if(type == 0){
			that.setData({findCarPage:0,hasMoreCar:true});
		}else{
			that.setData({findPeoplePage:0,hasMorePeople:true});
		}
		that.getCarpoolList(parseInt(that.data.tabIndex)+1);
		wx.stopPullDownRefresh();
	},
	onReachBottom: function () {
    	//上拉加载新数据  
    	var type = parseInt(that.data.tabIndex);
    	if(type == 0 && !this.data.hasMoreCar){//人找车
    		wx.stopPullDownRefresh();
	      	return;
    	}
	    if (type == 1 && !this.data.hasMorePeople) {
	      wx.stopPullDownRefresh();
	      return;
	    }
	    this.getCarpoolList(this.data.tabIndex+1);
    },
  pushFormSubmit:function(e){	
		console.log(e.detail.formId);
		wx.navigateTo({
	      url: e.currentTarget.dataset.url
	    }) 
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})