<!--人找车-->
<template name="find-car-list">
<view>
	<view wx:for="{{findCarList}}" wx:key="*this" wx:for-item="item" wx:for-index="index">
		<form bindsubmit="pushFormSubmit"  report-submit='true' data-url="/pages/car/home/detail?id={{item.id}}&type={{item.type}}" >
		<view class="scheduleitem tapStyle">				
				<button class="form_btn" plain="true"  formType="submit" ></button>

				<view class="info padB30 clear borderBottom">
					<image class="floatL"  src="../../../image//userinfo.png">
					</image>
					<view class="floatL">
						<view> <text class="textover" style="width:150px">{{item.nickname}}</text></view>
					    <text class="carInfo textover" style="width:150px">发布于 {{item.createTime}}</text>
					</view>
					<view class="floatR">
						<view style="background-color: #20C826;color:#fff;padding: 4px 10px;text-align:center;border-radius: 4px;"> 人找车</view>
					</view>	
				</view>
				<view class="item clear">
			   		<text class="remark">{{item.remark}} </text>
			   </view>
			   
		</view>
		</form>
	</view>
</view>
</template>

<template name="find-people-list">
<view>
	<view wx:for="{{findPeopleList}}" wx:key="*this" wx:for-item="item" wx:for-index="index">
		<form bindsubmit="pushFormSubmit"  report-submit='true' data-url="/pages/car/home/detail?id={{item.id}}&type={{item.type}}" >
		<view class="scheduleitem tapStyle">
				<button class="form_btn" plain="true"  formType="submit" ></button>
				<view class="info padB30 clear borderBottom">
					<image class="floatL"  src="../../../image//userinfo.png">
					</image>
					<view class="floatL">
						<view> <text class="textover" style="width:150px">{{item.nickname}}</text></view>
					    <text class="carInfo textover" style="width:150px">{{item.createTime}}</text>
					</view>
					<view class="floatR">
						<view style="background-color: #F56250;color:#fff;padding: 4px 10px;text-align:center;border-radius: 4px;"> 车找人</view>
					</view>	
				</view>
				<view class="item clear">
			   		<text class="remark">{{item.remark}} </text>
			   </view>			   
		</view>
		</form>
	</view>
</view>
</template>