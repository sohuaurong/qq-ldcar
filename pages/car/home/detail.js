const _ = require('../../../utils/underscore');
var util = require('../../../utils/util.js');
var CryptoJS = require('../../../utils/encry/tripledes.js').CryptoJS;
import {get, post, put, del} from '../../../utils/network';
var app = getApp();
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
	user:{},
  	detail:{},//详情信息
  	type:1,
  	isLogin:true
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	console.log(options);
	if(!util.isStrEmpty(options.type) && options.type == 1){
			qq.setNavigationBarTitle({title: "乘客行程详情"})
		}else if(options.type == 2){
		  	qq.setNavigationBarTitle({title: "司机行程详情"})
		}
	this.setData({id:options.id});
	this.getDetail(options.id);
  },
  getDetail:function(id){
  	that = this;
	get('v1/simple/carpool/detail',{id:id}).then(res=>{
		console.log(res);
		res.createTime = util.formatTime(new Date(res.createTime));
		that.setData({detail:res});
	})
  },
  invitationPre:function(e){
  		app.collectFormId(e);
  		that = this;
  		//判断有没有登录
  		if(app.globalData.isLogin){
  				that.invitation(e);
  		}else{
  			
  		}
  },
	/**
	 * 邀请同行
	 */
	invitation:function(e){
		console.info('点击了邀请同行');		
		that = this;		

	},
  cancelTap:function(){
  	this.setData({
      showDialog: false
    });
  },
  onCallPhoneTap: function (e) {
	if(!app.globalData.isLogin){
		qq.showModal({
		  title: '提示',
		  content: '请先登录再使用！',
		  success(res) {
		    if (res.confirm) {
		       qq.navigateTo({
		         url: '/pages/authorize/index'
		       })
		    } else if (res.cancel) {
		      console.log('用户点击取消')
		    }
		  }
		})
	}else{
		//拨打电话
		const mobile = e.currentTarget.dataset.mobile;	
		qq.makePhoneCall({
		  phoneNumber: mobile
		})
	}

  },
  goHome: function(t) {
  		var pages=getCurrentPages();
  		if(pages.length>1){
  			qq.navigateBack({
		      success: function (res) {
		        console.info('返回上一页');
		      }
		    });
  		}else{
  			qq.switchTab({
		            url: "/pages/car/home/index"
		        });
  		}
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function (ops) {
  	
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  	that = this;
  	that.getDetail(that.data.id);
  	wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
  	if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
  	console.info('分享出去的id:'+this.data.id);
  	var userId = app.globalData.userId;
    return {
      title: '罗定老乡都在用的拼车小程序',
      desc: "罗定老乡都在用的拼车小程序",
      path: '/pages/car/home/detail?id='+this.data.id+'&userId='+userId,
      success: function(res) {
        // 转发成功
        console.info('onShareAppMessage分享成功，'+JSON.stringify(res))


      },
      fail: function(res) {
        // 转发失败
        console.log('分享失败:'+JSON.stringify(res))
      }
    }
  }
  
})