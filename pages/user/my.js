var app = getApp();
var util = require('../../utils/util.js');
import {get, post, put, del} from '../../utils/network';
var that;
Page({

  /**syncWechatInfo 
   * 页面的初始数据
   */
  data: {
  	userInfo: {},
  	carshow:true,
  	isshow:false,
  	syncWechatInfo:false,
  	isLogin:true,
  	userAudit:'未认证',
  	driverAudit:'未认证',
	appid:app.globalData.appid,
	loginTck: false,//登录弹框
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
			
  },
  getDriverInfo:function(){
  	that = this;
	get('/v1/user/driver/info/get').then(res=>{
		console.log(res);
		if(res.mobileFlag){
			that.setData({userAudit:'已认证'});
		}
		var driverAudit = '未认证';
		if(res.auditFlag == 1){
			driverAudit = '已认证';
		}else if(res.auditFlag == 2){
			driverAudit = '审核中';
		}else if(res.auditFlag == 3){
			driverAudit = '认证不通过';
		}
		that.setData({driverAudit:driverAudit});
	})
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

	// 登录弹框
	loginClick: function () {
		if(app.globalData.isLogin){
			return;
		}
		this.setData({
			loginTck: (!this.data.loginTck)
		})
	},
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  	that = this;
  	if(app.globalData.isLogin){ 
		console.info(app.globalData.userInfo.headImg);
		var userInfo = app.globalData.userInfo;
		if(util.isOptStrNull(userInfo.headImg)){
			userInfo.headImg = '../../image/headimg.jpg';
		}  			
		that.setData({userInfo: userInfo});		
		that.getDriverInfo();
  	}else{
		//先登录一下
		qq.login({
			success(res) {
				if (res.code) {
					get('/auth/openuser/'+app.globalData.appid+'/login',{code:res.code,platform:'QQ'}).then(res=>{
						console.log(res);
						app.globalData.token = res.token;
						app.globalData.userInfo = res.user;
						app.globalData.isLogin = true;
						that.setData({
							userInfo:res.user
						});
						that.getDriverInfo();
					})
				} else {
					console.log('登录失败！' + res.errMsg)
				}
			}
		})
		var userInfo = new Object();
		userInfo.nickname = '点击登录';
		that.setData({userInfo: userInfo});
  	}
  	
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
	  console.log('==========pull');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  getUserInfo:function(e){
	  console.log(e);
	  that = this;
  	if(e.detail.errMsg == 'getUserInfo:fail auth deny'){				
		return;
	}
  	console.info(e.detail.userInfo);
	that.setData({
	            loginTck: false
	        })
	qq.login({
		success(res) {
			if (res.code) {
				let reqData = {
					appid:app.globalData.appid,
				    code: res.code,
				    encryptedData: e.detail.encryptedData,
				    iv: e.detail.iv,
					platform:'QQ'
				}
				post('auth/openuser/'+app.globalData.appid+'/register',reqData).then(res=>{
					console.log(res);
					app.globalData.token = res.token;
					app.globalData.userInfo = res.user;
					app.globalData.isLogin = true;
					that.setData({
						userInfo:res.user
					});
				})
			} else {
				console.log('登录失败！' + res.errMsg)
			}
		}
	})		
  },
  /**
	 * 展开或收缩
	 */
	onToggleTap: function (e) {
		that = this;
	    const dataset = e.currentTarget.dataset, name = dataset.name;
	    if('qita' == name){
	    		that.setData({isshow:!that.data.isshow});
	    }else if('car' == name){
	    		that.setData({carshow:!that.data.carshow});
	    }
	},

	 /**
	 * 跳转页面
	 */
	onNavigateTap: function (e) {
	    const dataset = e.currentTarget.dataset, url = dataset.url, name = dataset.name;
	    that = this;
	    console.info('==='+name);
		if(!app.globalData.isLogin){
			qq.showToast({ title: '请先登录', icon: 'loading', duration: 1000 });
			return;
		}
	    if ("wechat_address" == name) {
	        wx.chooseAddress({});
	    } else if ("wechat_setting" == name) {
	        qq.openSetting({});
	    } else if ("wechat_clear" == name) {
	        qq.showToast({ title: '正在清理中...', icon: 'loading', duration: 10 });
	        qq.clearStorageSync();
	        qq.showToast({ title: '清理完成', icon: 'success', duration: 1500 });
	    } else if ('pinche_my_route' == name) {
	        qq.switchTab({
					  url: url
					})
	    } else if ('wechat_info_sync' == name) {
	        //that.onSyncWechatInfo();
	    } else {
	        qq.navigateTo({ url: url });
	    }
	},
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})