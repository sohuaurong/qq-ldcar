var app = getApp();
var area = require('../../../data/gd.js');
var p = 0, c = 0, d = 0;
var util = require('../../../utils/util.js');
var dialog = require('../../../utils/dialog.js');
import {get, post, put, del} from '../../../utils/network';
var that;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    genderArr: ['男', '女'],
    index: 0,
    genderIndex:1,
    townships: ['罗城', '素龙','泗纶', '附城', '双东', '罗镜', '太平', '分界', '罗平', '船步', '塘镇', '苹塘', '金鸡', '围底', '华石', '榃滨', '黎少', '生江', '连州',  '加益', '龙湾'], 
    town_index: 0,
    name:'罗城',
    provinceName:[],
    provinceCode: [],
    provinceSelIndex: '',
    cityName: [],
    cityCode: [],
    citySelIndex: '',
    districtName: [],
    districtCode: [],
    districtSelIndex: '',
    showMessage: false,
    messageContent: '',
    showDistpicker: false,
    userInfo:{},
    loadWorkCity:'',
    changeWorkCity:false,
    checkNickname:true,
    code:''//登录获取到的code
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  	that = this;
    that.setAreaData();
    get('v1/user/passenger/info/get').then(res=>{
      console.log(res);
      that.setData({
			userInfo: res,
			province:res.province,
			city:res.city,
			district:res.district,
			gender:parseInt(res.gender)
      });
      //回显原来的工作城市信息
      var city = "";
      if(!util.isOptStrNull(res.province)){
      	 city += that.data.userInfo.province+" ";
      }
      if(!util.isOptStrNull(res.city)){
      	 city += that.data.userInfo.city+" ";
      }
      if(!util.isOptStrNull(res.district)){
      	 city += that.data.userInfo.district;
      }
      console.info('---city:'+city);
      if(!util.isOptStrNull(city)){
      	that.setData({
      		loadWorkCity:city
      	});
      }
	  var mytown = res.townShip;
	  for(var i = 0;i<that.data.townships.length;i++){
	  	if(mytown == that.data.townships[i]){
	  			that.setData({town_index:i});
	  			break;
	  	}
	  }
	  
  })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },
  savePersonInfo:function(e){
  	that = this;
  	var data = e.detail.value;
    console.info('表单数据:'+JSON.stringify(data));
    var telRule = /^1[3|4|5|7|8]\d{9}$/, 
    //nameRule = /^[\u2E80-\u9FFF]+$/
    nameRule = /^[\u2E80-\u9FFF]{2,4}[-,——][\u2E80-\u9FFF]{2,4}[-,——](.){1,20}$/;
    if(util.isStrEmpty(data.nickname)){
    	dialog.showTopTips('请填写您的昵称！', that);
	    return;
    }
    if(that.data.checkNickname && !nameRule.test(data.nickname)){
		dialog.showTopTips('您的昵称格式不符合！(格式：南山-罗城-XX)', that);
	    return;
    }
    if(util.isStrEmpty(data.mobile)){
    	dialog.showTopTips('请填写您的手机号！', that);
	    return;
    }
    if(!telRule.test(data.mobile)){
    	dialog.showTopTips('您的手机号码格式不正确！', that);
	    return;
    }
    
    if(util.isStrEmpty(data.province) || util.isStrEmpty(data.city) || (data.district != '东莞市' && util.isStrEmpty(data.district))){
    	dialog.showTopTips('请选择您的工作城市！', that);
	    return;
    }
    if(util.isStrEmpty(data.townShip)){
    	dialog.showTopTips('请选择您的乡镇！', that);
	    return;
    }
    
    post('v1/user/passenger/info/modify',data).then(res=>{
      console.log(res);
	  qq.showToast({
	    title: '保存成功',
	    icon: 'success',
	    duration: 2000
	  })
	  qq.navigateBack();
    }) 
	 
  },
  onDataChange: function (e) {
    //变更绑定的值
    const name = e.currentTarget.dataset.name;
    const info = {};
    info[name] = e.detail.value;
    this.setData(info);
  },
  onMobileBlur: function (e) {
      //获取手机号
      this.mobile = e.detail.value;
    },
  /**
   * 所属乡镇
   */
	bindPickerTownChange:function(e){
  	this.setData({
          town_index: e.detail.value,
      })
  },
  setAreaData: function(p, c, d){
    var p = p || 0 // provinceSelIndex
    var c = c || 0 // citySelIndex
    var d = d || 0 // districtSelIndex
    // 设置省的数据
    var province = area['100000'];
    var provinceName = [];
    var provinceCode = [];
    for (var item in province) {
      provinceName.push(province[item]);
      provinceCode.push(item);
    }
    this.setData({
      provinceName: provinceName,
      provinceCode: provinceCode
    });
    // 设置市的数据
    var city = area[provinceCode[p]];
    var cityName = [];
    var cityCode = [];
    for (var item in city) {
    	//console.info('city:'+item);
      cityName.push(city[item]);
      cityCode.push(item);
    }
    this.setData({
      cityName: cityName,
      cityCode: cityCode
    })
    // 设置区的数据
    var district = area[cityCode[c]];
    var districtName = [];
    var districtCode = [];
    for (var item in district) {
      districtName.push(district[item])
      districtCode.push(item)
    }
    this.setData({
      districtName: districtName,
      districtCode: districtCode
    })
  },
  changeArea: function(e) {
    p = e.detail.value[0];
    c = e.detail.value[1];
    d = e.detail.value[2];
    console.info("p:"+p+"c:"+c);
    this.setAreaData(p, c, d)
  },
  showDistpicker: function() {
    this.setData({
      showDistpicker: true
    })
  },
  distpickerCancel: function() {
    this.setData({
      showDistpicker: false
    })
  },
  distpickerSure: function() {
    this.setData({
      provinceSelIndex: p,
      citySelIndex: c,
      districtSelIndex: d,
      changeWorkCity:true
    });
    this.distpickerCancel();
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  getPhoneNumber: function(e) { 
        that = this;
        if (!e.detail.errMsg || e.detail.errMsg != "getPhoneNumber:ok") {
			      wx.showModal({
			        title: '提示',
			        content: '无法获取手机号码',
			        showCancel: false
			      })
			      return;
		    }
        var params = {code:that.data.code,"encryptedData": e.detail.encryptedData,"iv": e.detail.iv};
				netUtil.buildRequest(that, '/user/getPhone', params, {								
								onSuccess: function(data) {									
									that.setData({'userInfo.mobile':data});
								},
								onError: function(msgCanShow, code, hiddenMsg) {
									console.log('无法获取到手机:'+msgCanShow);
									netUtil.alertDialog("无法获取到手机");
								}
							}).send();
  } 
})