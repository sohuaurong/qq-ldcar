<view class="weui-toptips weui-toptips_warn" style="background-color: rgba(0, 0, 0, .75);" wx:if="{{showTopTips}}">{{ paramsErrorMsg }}</view>
<view>
	<text class="list">注意事项:</text>
	<view class="list">
		1、发布拼车信息请先完善个人信息，<text style="color:red">昵称格式：所在城市-老家城镇-名字</text>。
	</view>
</view>
<form report-submit="true" bindsubmit="savePersonInfo" bindreset="formReset">

  <!--手机号、姓名-->
  <view class="mb15">

    <!--姓名-->
    <view class="list flex tr gridXb bgwhite">
      <view class="tl">
        <text class="f16 mr10 grey iconfont icon-user-outline"></text>昵称</view>
      	<input type="text" class="sub" name="nickname" maxlength="30" value="{{userInfo.nickname}}" placeholder="格式：南山-罗城-XX" />
    </view>

    <!--性别-->
    <view class="list flex tr gridXb bgwhite">
      <view class="tl sub">
        <text class="f16 mr10 grey iconfont icon-female"></text>性别
      </view>
      <input type="text" hidden="true" name="gender" value="{{gender===undefined?1:gender}}" />
      <radio-group class="radio-group" bindchange="onDataChange" data-name="gender">
        <label class="mr20">
          <radio value="1" checked="true" /> 男</label>
        <label>
          <radio value="2" checked="{{userInfo.gender == 2}}" />女</label>
      </radio-group>

    </view>

    <!--手机号-->
    <view class="list flex tr gridXb bgwhite">
      <view class="tl">
        <text class="f16 mr10 grey iconfont icon-phone"></text>手机号</view>
      <input type="number" name="mobile" value="{{userInfo.mobile}}" class="sub" bindblur="onMobileBlur" placeholder="请填写您的手机号" maxlength='11'/>
    </view>
    
  </view>
  
  <view class="mb15">
    	<!--工作城市 -->
    <view class="list flex tr gridXb bgwhite">
      <view class="sub tl">
        <text class="f16 grey mr5 iconfont icon-locationfill"></text> 工作城市</view>
      	<view class="form-item-bd" bindtap="showDistpicker">
                <view class="input {{!provinceName[provinceSelIndex] ? 'input-placeholder' : ''}}">
                    <block wx:if="{{provinceName[provinceSelIndex]}}">{{provinceName[provinceSelIndex]}} {{cityName[citySelIndex]}} {{districtName[districtSelIndex]}}</block>
                    <block wx:else>
                    	<text style="clolr:black;">{{loadWorkCity}}</text>
                    	<text wx:if="{{!loadWorkCity}}">请选择工作城市</text>
                    </block>
                </view>                
                
                <input type="text" name="province" value="{{provinceName[provinceSelIndex] || province}}" class="hidden"/>
                <input type="text" name="city" value="{{cityName[citySelIndex] || city}}" class="hidden"/>
                <input type="text" name="district" value="{{districtName[districtSelIndex] || district}}" class="hidden"/>               
                
         </view>
    </view>
    <!--罗定乡镇 -->
	<view class="list flex tr gridXb bgwhite">
	      <view class="tl sub">
	        <image src="/image/endCity.png" style="vertical-align: middle;width:15px; height: 15px;"></image>
	        <text class="f16 mr10 grey"></text>老家城镇
	      </view>
	      <view class="form-item-bd">
	                <picker bindchange="bindPickerTownChange" value="{{town_index}}" range="{{townships}}" >
	                    <view class="picker" style="margin-top:5px;margin-left:5px">
	                       	 {{townships[town_index]}}
	                    </view>
	                    <input type="text" name="townShip" value="{{townships[town_index]}}" class="hidden"/>
	                </picker>
	            </view>
	</view> 
    
  </view>
  <button form-type="submit" type="primary" hover-class="none" class="m15">提交信息</button>
</form>

<!--城市选择器 -->
  <view class="distpicker {{showDistpicker ? '' : 'hidden'}}" wx:if="{{showDistpicker}}">
    <view class="distpicker-btn">
        <view class="distpicker-cancel-btn" bindtap="distpickerCancel">取消</view>
        <view class="distpicker-sure-btn" bindtap="distpickerSure">确定</view>
    </view> 
    <picker-view indicator-style="height: 40px;" class="distpicker-content" value="{{value}}" bindchange="changeArea">
        <!-- 省 -->
        <picker-view-column>
            <view wx:for="{{provinceName}}" wx:key="province" class="area-item">{{item}}</view>
        </picker-view-column>
        <!-- 市 -->
        <picker-view-column>
            <view wx:for="{{cityName}}" wx:key="city" class="area-item">{{item}}</view>
        </picker-view-column>
        <!-- 区 -->
        <picker-view-column>
            <view wx:for="{{districtName}}" wx:key="district" class="area-item">{{item}}</view>
        </picker-view-column>
    </picker-view>
</view>
<view class="mask" catchtap="distpickerCancel" hidden="{{!showDistpicker}}"></view>