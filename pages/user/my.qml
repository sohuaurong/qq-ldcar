<!-- 顶部头像 -->
<view class="user_content">
         <view class="user-elembg">
            <image class="user-elem" src="http://ixiaochengxu.cc/resource/images/user/element-bg.png" mode='aspectFit'></image>
            <image class="user-elem" src="http://ixiaochengxu.cc/resource/images/user/element-bg.png" mode='aspectFit'></image>
        </view> 
        <view class="Member_head">
            <view catchtap="loginClick">
            <image class="userinfo-avatar"  src="{{ userInfo.headImg}}" mode="aspectFill"></image>
            </view>
            <view class="userinfo_name-box" >
                <view class="userinfo_name lineText" catchtap="loginClick">{{userInfo.nickname}}</view>
                <!-- <view class="tongbu-btn"  data-name="wechat_info_sync">
					<button open-type="getUserInfo" bindgetuserinfo="getUserInfo" class="iconfont icon-tongbuxinxi usersync"></button>                
                </view> -->
            </view>
        </view>		
</view>

<!-- 菜单部分-->
<view class="my-menu">
		
		<!-- carpool start-->
		<view class="my-menu-box">
			<view class="my-menu-item {{carshow == true ? 'arrowR arrowRv' :'arrowR'}}" bindtap="onToggleTap" data-name="car">
	            <text class="iconfont icon-duoguan-pinche mr10"></text>
	            <text>拼车信息</text>
        	</view>
        	<view class="forum-box" style="display:{{carshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="pinche_my_info" data-url="/pages/user/user_edit/index">
					<text class="arrowL">个人信息</text>
					<text class="info-binding">{{userAudit}}</text>
				</view>
			</view>
			<view class="forum-box" style="display:{{carshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="pinche_driver_info" data-url="/pages/user/driver_edit/index">
					<text class="arrowL">车主认证</text>
					<text class="info-binding">{{driverAudit}}</text>
				</view>
			</view>
        	<!-- <view class="forum-box" style="display:{{carshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="pinche_my_route" data-url="/pages/car/mine/index">
					<text class="arrowL">我的行程</text>
				</view>
			</view> -->
		</view>
		<!-- carpool end-->
		
		<!-- 其他服务-->
		<view class="my-menu-box">
			<view class="my-menu-item {{isshow == true ? 'arrowR arrowRv' :'arrowR'}}" bindtap="onToggleTap" data-name="qita">
	            <text class="iconfont icon-xitongshezhi mr10"></text>
	            <text>其他</text>
        	</view>
        	<!-- <view class="forum-box" style="display:{{isshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="settings" data-url="/pages/car/help/index">
					<text class="arrowL">常见问题</text>
				</view>
			</view>
			
        	<view class="forum-box" style="display:{{isshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="settings" data-url="/pages/user/feedback/index">
					<text class="arrowL">意见反馈</text>
				</view>
			</view> -->
			
			<view class="forum-box" style="display:{{isshow?'block':'none'}}">
				<view class="my-menu-subitem gridXt" catchtap="onNavigateTap" data-name="settings" data-url="/pages/user/setting/index">
					<text class="arrowL">系统设置</text>
				</view>
			</view>
			
		</view>
		
</view>


<!-- 登录提示-->
<view class="loginTck" wx:if="{{loginTck}}">
  <view class="tckBg" bindtap="loginClick"></view>
  <view class="loginBox">
    <image src="http://static.ixiaochengxu.cc/mofang/images/user/notLogin.png" mode='aspectFit'></image>
    <view class="loginTitle">为了您更好的体验小程序，请先登录</view>
    <view class="loginBtn gridXt flex">
      <button class="sub gridYr" bindtap="loginClick">取消</button>
      <button class="sub sureBtn" type="primary" open-type="getUserInfo"  bindgetuserinfo="getUserInfo">确定</button>
    </view>
  </view>
</view>