<view class="weui-toptips weui-toptips_warn" style="background-color: rgba(0, 0, 0, .75);" wx:if="{{showTopTips}}">{{ paramsErrorMsg }}</view>
<view>
	<text class="list">注意事项:</text>
	<view class="list">
		1、车主信息认证，请如实填写，审核通过后，姓名以及身份证信息将不能修改。
	</view>
</view>
<form report-submit="true" bindsubmit="onPushSubmit" bindreset="formReset">

  <!--手机号、姓名-->
  <view class="mb15">

    <!--姓名-->
    <view class="list flex tr gridXb bgwhite">
      <view class="tl">
        <text class="f16 mr10 grey iconfont icon-user-outline"></text>真实姓名</view>
      	<input type="text" class="sub" name="realname" maxlength="30" value="{{ driverInfo.realname }}" placeholder="请填写您的真实姓名" />
    </view>
	
	<view class="list flex tr gridXb bgwhite">
      <view class="tl">
      	<image src="/image/id-card.svg" style="vertical-align: middle;width:15px; height: 15px;"></image>
        <text class="f16 mr10 grey"></text>身份证号码</view>
      	<input type="idcard" class="sub" name="identitycard" maxlength="30" value="{{ driverInfo.identitycard }}" placeholder="请填写您的身份证号" />
    </view>
    
  </view>

  <!--车型颜色、车牌号码-->
  <view class="mb15">

    <!--姓名-->
    <view class="list flex tr gridXb bgwhite">
      <view class="tl">
      <image src="/image/chexing.svg" style="vertical-align: middle;width:15px; height: 15px;"></image>
        <text class="f16 mr10 grey"></text>车型颜色</view>
      	<input type="text" class="sub" name="carInfo" maxlength="30" value="{{ driverInfo.carInfo }}" placeholder="请填写您的车型、颜色" />
    </view>
	
	<view class="list flex tr gridXb bgwhite">
      <view class="tl">
      	<image src="/image/car-no.svg" style="vertical-align: middle;width:15px; height: 15px;"></image>
        <text class="f16 mr10 grey"></text>车牌号码</view>
      	<input type="text" class="sub" name="carNumber" maxlength="30" value="{{ driverInfo.carNumber }}" placeholder="请填写您的车牌号" />
    </view>
    
  </view>
  
  <button form-type="submit" type="primary" hover-class="none" class="m15">提交信息</button>
</form>