var app = getApp();
var util = require('../../../utils/util.js');
var dialog = require('../../../utils/dialog.js');
import {get, post, put, del} from '../../../utils/network';
var that;

Page({

    /**
     * 页面的初始数据
     */
    data: {
        driverInfo:{},
        mobileFlag:true//个人信息认证
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        that = this;
		get('v1/user/driver/info/get').then(res=>{
			console.log(res);
			that.setData({
			    driverInfo: res
			});
			if(!res.mobileFlag){
				wx.showModal({
					title: '提示',
					content: '请先完善个人手机信息',
					success: function (res) {
					  if (res.confirm) {
							qq.navigateTo({
							  url: '/pages/user/user_edit/index'
							});
					  } else if (res.cancel) {
							qq.switchTab({
							  url: '/pages/car/home/index'
							});
					  }
					}
			  });
			}else{
				if(!util.isOptStrNull(res.identitycard)){
					if(res.auditFlag == 2){//认证中
						netUtil.alertDialog('你的车主身份正在认证中，请等候!');
					}else if(res.auditFlag == 3){
						netUtil.alertDialog('审核不通过,原因:'+ret.remark);
					}
				}else{
					console.info('第一次填写司机信息');
				}
			}
		})
        
    },
    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },
    onPushSubmit: function (e) {
        console.info(JSON.stringify(e));
        //表单提交
        var that = this;
        var formData = e.detail.value;
        console.info(JSON.stringify(formData));
        if(that.checkForm(formData)) {
            console.info('请求参数'+JSON.stringify(formData));
            post('v1/user/driver/info/audit',formData).then(res=>{
				console.log(res);
				qq.navigateBack();
			})
        }

    },
    checkForm:function(formData){
        if(formData.realname == '') {
            dialog.showTopTips('请输入您的真实姓名', this);
            return false;
        } else if(formData.identitycard == '') {
            dialog.showTopTips('请输入您的身份证号码', this);
            return false;
        }else if(formData.carInfo == ''){
            dialog.showTopTips('请填写您的车型', this);
            return false;
        }else if(formData.carNumber == ''){
            dialog.showTopTips('请填写您的车牌号', this);
            return false;
        }
        return true;
    },

})