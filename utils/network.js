const app = getApp();
/**
 * 发起get请求
 * @param url 请求路径 必填
 * @param data 请求参数 get请求的参数会自动拼到地址后面
 * @param headers 请求头 选填
 * @returns {Promise}
 */
export const get = (url, data, headers) => request('GET', url, data, headers);

/**
 * 发起post请求
 * @param url 请求路径 必填
 * @param data 请求参数
 * @param headers 请求头 选填
 * @returns {Promise}
 */
export const post = (url, data, headers) => request('POST', url, data, headers);
/**
 * 发起put请求
 * @param url 请求路径 必填
 * @param data 请求参数
 * @param headers 请求头 选填
 * @returns {Promise}
 */
export const put = (url, data, headers) => request('PUT', url, data, headers);
/**
 * 发起delete请求
 * @param url 请求路径 必填
 * @param data 请求参数 delete请求的参数会自动拼到地址后面
 * @param headers 请求头 选填
 * @returns {Promise}
 */
export const del = (url, data, headers) => request('DELETE', url, data, headers);

/**
 * 接口请求基类方法
 * @param method 请求方法 必填
 * @param url 请求路径 必填
 * @param data 请求参数
 * @param header 请求头 选填
 * @returns {Promise}
 */
export function request(method, url, data, header = {'Content-Type': 'application/json','Cookie':'APP_TOKEN='+app.globalData.token}) {
  console.info(method, url);
  console.log(app);
  console.info(app.globalData.token);
	url = app.globalData.API_ROOT+url;
	qq.showLoading({
	    title: '加载中',
	  })
    return new Promise((resolve, reject) => {
        const response = {};
        qq.request({
            url, method, data, header,
            //success: (res) => response.success = res.data,
			success: (res) => {
				qq.hideLoading();
				if (res.data.code == 0) { //判断code值
					response.success = res.data.data || {}; // 你将在then方法中看到的数据
				} else if(res.data.code == 50000){//&& url.indexOf("login") == -1
					console.log('aaaaaaaaaaaalogin');
					var pages = getCurrentPages();
					if(pages.length>0){
						var curragePage = pages[pages.length-1];
						var url = curragePage.route;
						console.log(url);
						if(url != 'pages/authorize/authorize'){
							//qq.navigateTo({url:'/pages/authorize/authorize'}); // 跳去登录页
						}
					}
				} else {
					if(res.data.code != 50000){
						qq.showModal({
						  title: '提示',
						  content: res.data.message,
						  showCancel:false
						});
					}					
					response.fail = res.data; // 你将在catch方法中接收到该错误
				}
			},
            fail: (error) => response.fail = error,
            complete() {
                console.group('==============>请求开始<==============');
                console.info(method, url);
                if (response.success) {
                    console.info('请求成功', response.success);
                    resolve(response.success)
                } else {
                    console.info('请求失败', response.fail);
                    reject(response.fail)
                }
                console.info('==============>请求结束<==============');
                console.groupEnd();
				qq.hideLoading();
            },
        });
    });
}