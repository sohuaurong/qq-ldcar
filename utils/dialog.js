function showTopTips(msg,that){
	console.info('错误信息:'+msg);
	that.setData({
		showTopTips: true,
        paramsErrorMsg:msg
	});
	setTimeout(function() {
		that.setData({
			showTopTips: false,
            paramsErrorMsg:''
		});
	}, 3000);
}

module.exports = {
  showTopTips: showTopTips
}